﻿using System;
using System.Diagnostics;
using System.Threading;

namespace PerfomanceOfMultiThreadedPrograms
{
    class Program
    {
        public static void EvenNumbeSum()
        {
            double sum = 0;
            for (int i = 0; i <= 5000000; i++)
            {
                if (i % 2 == 0)
                    sum += i;
            }
            Console.WriteLine("Even number sum is : " + sum);
        }
        public static void OddNumbeSum()
        {
            double sum = 0;
            for (int i = 0; i <= 5000000; i++)
            {
                if (i % 2 == 1)
                    sum += i;
            }
            Console.WriteLine("Odd number sum is : " + sum);
        }
        static void Main(string[] args)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            EvenNumbeSum();
            OddNumbeSum();

            stopwatch.Stop();
            Console.WriteLine(" Total milliseconds wihtout using threads : " + stopwatch.ElapsedMilliseconds);

            stopwatch = Stopwatch.StartNew();
            Thread t1 = new Thread(EvenNumbeSum);
            t1.Start();

            Thread t2 = new Thread(OddNumbeSum);
            t2.Start();

            t1.Join();
            t2.Join();
            stopwatch.Stop();
            Console.WriteLine("Total milliseconds using threads :" + stopwatch.ElapsedMilliseconds);

            Console.ReadLine();
        }
    }
}
