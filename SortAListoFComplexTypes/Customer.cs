﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAListoFComplexTypes
{
    class Customer /*: IComparable<Customer>*/
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }

        #region Part 79
        //public int CompareTo(Customer other)
        //{
        //    //if(this.Salary > other.Salary)
        //    //    return 1;
        //    //else if(this.Salary < other.Salary)
        //    //    return -1;
        //    //else
        //    //    return 0;
        //    return this.Salary.CompareTo(other.Salary);
        //}
        #endregion
    }
}
