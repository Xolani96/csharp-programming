﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace SortAListoFComplexTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Salary = 1000,
            };

            Customer cust2 = new Customer()
            {
                ID = 3,
                Name = "Simphiwe",
                Salary = 5500,
            };

            Customer cust3 = new Customer()
            {
                ID = 2,
                Name = "Kopano",
                Salary = 4511,
            };

            List<Customer> ListCustomer = new List<Customer>(100);
            ListCustomer.Add(cust1);
            ListCustomer.Add(cust2);
            ListCustomer.Add(cust3);

            //TrimExcess
            Console.WriteLine("Capacity before trimming " + ListCustomer.Capacity);
            ListCustomer.TrimExcess();
            Console.WriteLine("Capacity After trimming " + ListCustomer.Capacity);

            //Read only method
            //ReadOnlyCollection<Customer> readonlyCustomer = ListCustomer.AsReadOnly();
            //Console.WriteLine($"The total count is {readonlyCustomer.Count()}");

            //true for all method
            //bool answer = ListCustomer.TrueForAll(x => x.Salary > 300);
            //Console.WriteLine(answer);

            #region Part 79
            ////approach 1
            ////Comparison<Customer> customerComparer = new Comparison<Customer>(CompareCustomer);

            //Console.WriteLine("Before sorting");
            //foreach (var item in ListCustomer)
            //{
            //    Console.WriteLine(item.ID);
            //}

            //ListCustomer.Sort((x,y) => x.ID.CompareTo(y.ID)); //customerComparer - approach 1 
            ////delegate (Customer c1, Customer c2) { return c1.ID.CompareTo(c2.ID); - approach 2

            //Console.WriteLine("After sorting");
            //foreach (var item in ListCustomer)
            //{
            //    Console.WriteLine(item.ID);
            //}
            #endregion
            #region Part 78
            //Console.WriteLine("Before sorting");
            //foreach (var item in ListCustomer)
            //{
            //    Console.WriteLine($"Salary: {item.Salary}");
            //}

            //ListCustomer.Sort();
            //ListCustomer.Reverse();
            //Console.WriteLine("After sorting");
            //foreach (var val in ListCustomer)
            //{
            //    Console.WriteLine($"Salary: {val.Salary}");
            //}

            //SortByName sbn = new SortByName();
            //ListCustomer.Sort(sbn);
            //Console.WriteLine("sort by name");
            //foreach (var val in ListCustomer)
            //{
            //    Console.WriteLine($"Salary: {val.Salary}");
            //}
            #endregion

            Console.ReadLine();
        }
        #region Part 79
        //approach 1
        //private static int CompareCustomer(Customer x, Customer y)
        //{
        //    return x.ID.CompareTo(y.ID);
        //}
        #endregion
    }
}
