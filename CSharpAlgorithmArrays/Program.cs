﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAlgorithmArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //JaggedArray();
            //MultiDimArray();
            //Test1BasedArray();
            //ArraysDemo();
            //Iterate(new[] { 1, 2, 3 });

            Console.ReadLine();
        }
        private static void ArrayTimeComplexity(object[] array)
        {
            //access by index O(1)  
            Console.WriteLine(array[0]);

            int length = array.Length;
            object neededElement = new object();

            //searching for an element takes O(n)
            for (int i = 0; i < length; i++)
            {
                if(array[i] == neededElement)
                {
                    Console.WriteLine("Exists");
                }
            }

            //need to add into a full array
            var bigArray = new int[length * 2]; //double the length
            Array.Copy(array, bigArray, length);
            bigArray[length + 1] = 10; // adding an element in the newly created space

            //add to the end when there is some space
            //O(1)
            array[length - 1] = 10;

            //remove an element by assinging null O(1)
            array[2] = null;
        }
        private static void RemoveAt(object[] array, int index)
        {
            var newArray = new object[array.Length - 1];
            Array.Copy(array, 0, newArray, 0, index); // copy the values before the removed index
            Array.Copy(array, index + 1, newArray, index, array.Length - 1 - index); // copy the values before the removed index
        }
        private static unsafe void Iterate(int[] array)
        {
            fixed(int* b = array) //fixed : required when you want to pin a managed object
            {
                int* p = b;
                for (int i = 0; i < array.Length; i++)
                {
                    Console.WriteLine(*p); // *p dereferrence the pointer
                    p++;
                }
            }
        }
        private static void MultiDimArray()
        {
            //int[,] r1 = new int[2, 3] { { 1, 2, 3 }, { 4, 5, 6 } };
            int[,] r2 = { { 1, 2, 3 }, { 4, 5, 6 } };

            for (int i = 0; i < r2.GetLength(0); i++)
            {
                for (int j = 0; j < r2.GetLength(1); j++)
                {
                    Console.WriteLine($"{r2[i,j]}");
                }
                Console.WriteLine();
            }
        }
        private static void JaggedArray()
        {
            int[][] jaggedArray = new int[4][]; // you have 4 rows
            jaggedArray[0] = new int[1]; //row number 1 has 1 element
            jaggedArray[1] = new int[3]; //row number 2 has 3 element
            jaggedArray[2] = new int[2]; //row number 3 has 2 element
            jaggedArray[3] = new int[4]; //row number 4 has 4 element

            Console.WriteLine("Enter the numbers for a jagged array?");

            for (int i = 0; i < jaggedArray.Length; i++) // it get the number of rows
            {
                for (int j = 0; j < jaggedArray[i].Length; j++) // jaggedArray[i].Length : we get the lenght of each row
                {
                    int st =Convert.ToInt32(Console.ReadLine());
                    jaggedArray[i][j] = st;
                }
            }
            Console.WriteLine(" ");
            Console.WriteLine("Printing the element:");

            for (int i = 0; i < jaggedArray.Length; i++) // it get the number of rows
            {
                for (int j = 0; j < jaggedArray[i].Length; j++) // jaggedArray[i].Length : we get the lenght of each row
                {
                    Console.Write(jaggedArray[i][j]);
                    Console.Write("\0");
                }
                Console.WriteLine("");
            }
        }
        private static void ArraysDemo()
        {
            int[] a1 = new int[5] { 4, 5, 6, 7, 8 };
            //for (int i = 0; i < a1.Length; i++)
            //{
            //    Console.WriteLine(a1[i]);
            //}
            foreach (int item in a1)
            {
                Console.WriteLine(item);
            }

            //You can also instanciate an array like this
            Array myArray = new int[5];

            Array myArray2 = Array.CreateInstance(typeof(int), 5);
            myArray2.SetValue(1, 0);
        }
        private static void Test1BasedArray()
        {
            Array myArray = Array.CreateInstance(typeof(int), new[] { 4 }, new[] { 1 });
            myArray.SetValue(2019, 1);
            myArray.SetValue(2020, 2);
            myArray.SetValue(2021, 3);
            myArray.SetValue(2022, 4);

            Console.WriteLine($"Starting index: {myArray.GetLowerBound(0)}");
            Console.WriteLine($"ending index: {myArray.GetUpperBound(0)}");

            for (int i = myArray.GetLowerBound(0); i <= myArray.GetUpperBound(0); i++)
            {
                Console.WriteLine($"{myArray.GetValue(i)} at index {i}");
            }
        }

    }
}
