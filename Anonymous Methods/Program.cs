﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anonymous_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> listOfEmployees = new List<Employee>()
            {
                new Employee{ID = 101, Name = "Mandisa"},
                new Employee{ID = 102, Name = "Steven"},
                new Employee{ID = 103, Name = "Mbali"}
            };

            #region Anonymous methods
            //step 2
            //Predicate<Employee> employeePredicate = new Predicate<Employee>(FindEmployee);

            //step 3
            //Employee employee = listOfEmployees.Find(/*x=> employeePredicate(x)*/ /*or*/ delegate (Employee x) { return x.ID == 102; });

            //Console.WriteLine($"Id: {employee.ID} Name: {employee.Name}");

            //int count = listOfEmployees.Count(x => x.Name.StartsWith("M"));
            //Console.WriteLine(count);
            #endregion

            #region Func

            //Func<Employee, string> selector = employee => "Name " + employee.Name;

            //IEnumerable<string> names = listOfEmployees.Select(employee => "Name " + employee.Name);

            //foreach (string item in names)
            //{
            //    Console.WriteLine(item);
            //}

            //overloaded version of Func

            Func<int, int, string> funcDelegate = (firstnumber, secondnumber) => "Sum " + (firstnumber + secondnumber).ToString();

            string result = funcDelegate(12, 30);
            Console.WriteLine("Result :" + result);
            #endregion

            Console.ReadLine();
        }

        #region Anonymous method : deleted method
        //step 1
        //public static bool FindEmployee(Employee emp)
        //{
        //    return emp.ID == 102;
        //}
        #endregion
    }
    public class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
