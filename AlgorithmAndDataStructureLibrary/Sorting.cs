﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmAndDataStructureLibrary
{
    public class Sorting
    {
        public static void BubbleSort(int[] array)
        {
            for (int partindex = array.Length - 1; partindex > 0; partindex--)
            {
                for (int i = 0; i < partindex; i++)
                {
                    if (array[i] > array[i + 1])
                        Swap(array, i, i + 1);

                }
            }
        }
        private static void Swap(int[] array, int i, int j)
        {
            if (i == j)
                return;
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        public static int Sum(int a, int b)
        {
            return a + b;
        }
    }
}
