﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdoDemo
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Customer c1 = new Customer();
            c1.FirstName = "Xolani";
            c1.LastName = "Nhlapo";

            string fullname1 = c1.fullname();
            Response.Write($"Fullname: {fullname1}" + "<br/>");

            //Partial class
            PartialCustomer c2 = new PartialCustomer();
            c2.FirstName = "Xolani";
            c2.LastName = "Nhlapo";

            string fullname2 = c2.fullname();
            Response.Write($"Fullname: {fullname2}" + "<br/>");

            Console.ReadKey();
        }
    }
}