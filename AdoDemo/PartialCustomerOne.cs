﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdoDemo
{
    public partial class PartialCustomer
    {
        private string _firstname;
        private string _lastname;

        public string FirstName
        {
            get
            {
                return _firstname;
            }
            set
            {
                _firstname = value;
            }
        }
        public string LastName
        {
            get
            {
                return _lastname;
            }
            set
            {
                _lastname = value;
            }
        }
    }
}