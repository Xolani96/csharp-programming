﻿using System;
using System.Threading;

namespace DeadLock
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main started");
            Account accountA = new Account(50000,101);
            Account accountB = new Account(1000, 102);

            AccountManager accountManagerA = new AccountManager(accountA, accountB, 10000);
            Thread t1 = new Thread(accountManagerA.transfer);
            t1.Name = "T1";

            AccountManager accountManagerB = new AccountManager(accountB, accountA, 5000);
            Thread t2 = new Thread(accountManagerB.transfer);
            t2.Name = "T2";

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Console.WriteLine("Main Completed");

            Console.ReadLine();
        }
    }
}
