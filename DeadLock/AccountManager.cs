﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DeadLock
{
    public class AccountManager
    {
        Account _fromAccount;
        Account _toAccount;
        double _amountToTransfer;

        public AccountManager(Account fromAccount, Account toAccount, double amountToTransfer)
        {
            this._fromAccount = fromAccount;
            this._toAccount = toAccount;
            this._amountToTransfer = amountToTransfer;
        }

        public void transfer()
        {
            object _lock1, _lock2;
            if(_fromAccount.ID < _toAccount.ID)
            {
                _lock1 = _fromAccount;
                _lock2 = _toAccount;
            }
            else
            {
                _lock2 = _fromAccount;
                _lock1 = _toAccount;
            }

            Console.WriteLine(Thread.CurrentThread.Name + " trying to acquire a lock on " + ((Account)_lock1).ID.ToString());
            lock (_lock1)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " acquire lock on " + ((Account)_lock1).ID.ToString());
                Console.WriteLine(Thread.CurrentThread.Name + " suspended for 2 second " );
                Thread.Sleep(2000);
                Console.WriteLine(Thread.CurrentThread.Name + " back in action and trying to acquire lock on " + ((Account)_lock2).ID.ToString());
                lock (_lock2)
                {
                    Console.WriteLine(Thread.CurrentThread.Name + " acquire lock on " + ((Account)_lock2).ID.ToString());
                    _fromAccount.Withdraw(_amountToTransfer);
                    _toAccount.Deposit(_amountToTransfer);
                    Console.WriteLine($"{Thread.CurrentThread.Name} Transfered {_amountToTransfer.ToString()} from {_fromAccount.ID.ToString()} to {_toAccount.ID.ToString()}");
                }
            }
        }
    }
}
