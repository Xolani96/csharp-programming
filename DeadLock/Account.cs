﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeadLock
{
    public class Account
    {
        double _balance;
        int _id;
        public Account(double Balance, int Id)
        {
            this._balance = Balance;
            this._id = Id;
        }

        public int ID
        {
            get { return _id; }
        }

        public void Withdraw(double amount)
        {
            _balance -= amount;
        }

        public void Deposit(double amount)
        {
            _balance += amount;
        }
    }
}
