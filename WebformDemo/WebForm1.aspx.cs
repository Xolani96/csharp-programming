﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebformDemo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CountriesData"] == null) // you want to load it once and use the information in the dictionary for the rest of the program
            {
                Country country1 = new Country() { Code = "ZAR", Name = "South Africa", Capital = "Pretoria" };
                Country country2 = new Country() { Code = "DZA", Name = "Algeria", Capital = "Algiers" };
                Country country3 = new Country() { Code = "AGO", Name = "Angola", Capital = "Luanda" };
                Country country4 = new Country() { Code = "BEN", Name = "Benin", Capital = "Porto-Novo" };
                Country country5 = new Country() { Code = "EGY", Name = "Egypt", Capital = "Cairo" };
                Country country6 = new Country() { Code = "KEN", Name = "Kenya", Capital = "Nairobi" };
                Country country7 = new Country() { Code = "MDG", Name = "Madagascar", Capital = "Antananarivo" };

                Dictionary<string, Country> DictionaryCountry = new Dictionary<string, Country>();
                DictionaryCountry.Add(country1.Code, country1);
                DictionaryCountry.Add(country2.Code, country2);
                DictionaryCountry.Add(country3.Code, country3);
                DictionaryCountry.Add(country4.Code, country4);
                DictionaryCountry.Add(country5.Code, country5);
                DictionaryCountry.Add(country6.Code, country6);
                DictionaryCountry.Add(country7.Code, country7);

                Session["CountriesData"] = DictionaryCountry; 
            }
        }

        protected void TxtCountryCode_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, Country> DicCountry = (Dictionary<string, Country>)Session["CountriesData"];
            Country foundcountry = DicCountry.ContainsKey(TxtCountryCode.Text.ToUpper()) ? DicCountry[TxtCountryCode.Text.ToUpper()] : null;

            if (foundcountry == null)
            {
                lblMessage.Text = "Country code not valid";
                TxtCountryCapital.Text = "";
                TxtCountryName.Text = "";
            }
            else
            {
                TxtCountryName.Text = foundcountry.Name;
                TxtCountryCapital.Text = foundcountry.Capital;
                lblMessage.Text = "";
            }
        } 
    }
}