﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinkLikeAProgrammer
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle();
            Console.ReadLine();
        }

        public static void Triangle()
        {
            int n = 5;
            int m = 0;

            for (int i = n; i > m; i--)
            {
                for (int j = i ; j > 0 ; j--)
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }
    }
}
