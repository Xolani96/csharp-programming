﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace OptionalParameters
{
    class Program
    {
        static void Main(string[] args)
        {
            //parameter array
            //AddNumber(1, 2, new object[] { 10, 10, 10 } /*or*/ /*10, 10, 10*/);

            //method overloading
            //AddNumber(2, 5);
            //AddNumber(2, 5, new int[] { 3, 4 }); 

            //specifying method default
            //Test(2, c:23);

            //Optional Parameters
            //AddNumber(2, 5, new int[] {2,6 }, new string[] { "Xolani", "Thabiso"});

            Console.ReadKey();
        }

        #region Optional parameters: Optional attribute
        public static void AddNumber(int firstNumber, int secondNumber, [Optional] int[] restOfTheNumbers, [Optional] string[] restOfTheWords)
        {
            int result = firstNumber + secondNumber;
            string words = string.Empty;
            if (restOfTheNumbers != null)
            {
                foreach (int item in restOfTheNumbers)
                {
                    result += item;
                }
                foreach (var item in restOfTheWords)
                {
                    words += item;
                }
            }
            Console.WriteLine(words);
            Console.WriteLine($"Sum: {result.ToString()}");
        }
        #endregion
        #region Specifying parameter default
        //public static void Test(int a, int b = 10, int c = 20)
        //{
        //    Console.WriteLine("a = " + a);
        //    Console.WriteLine("b = " + b);
        //    Console.WriteLine("c = " + c);
        //}
        //public static void AddNumber(int firstNumber, int secondNumber, int[] restOfTheNumbers = null )
        //{
        //    int result = firstNumber + secondNumber;
        //    if (restOfTheNumbers != null)
        //    {
        //        foreach (int item in restOfTheNumbers)
        //        {
        //            result += item;
        //        }
        //    }

        //    Console.WriteLine($"Sum: {result.ToString()}");
        //}
        #endregion
        #region Method overloading

        //public static void AddNumber(int firstNumber, int secondNumber)
        //{
        //    AddNumber(firstNumber, secondNumber, null);
        //}   
        //public static void AddNumber(int firstNumber, int secondNumber, int [] restOfTheNumbers)
        //{
        //    int result = firstNumber + secondNumber;
        //    if (restOfTheNumbers != null)
        //    {
        //        foreach (int item in restOfTheNumbers)
        //        {
        //            result += item;
        //        }
        //    }

        //    Console.WriteLine($"Sum: {result}");
        //}
        #endregion
        #region Optional parameter : parameters arrays
        //public static void AddNumber(int firstNumber, int secondNumber, params object[] restOfTheNumbers)
        //{
        //    int result = firstNumber + secondNumber;
        //    if (restOfTheNumbers != null)
        //    {
        //        foreach (int item in restOfTheNumbers)
        //        {
        //            result += item;
        //        }
        //    }

        //    Console.WriteLine($"Sum: {result}");
        //}
        #endregion
    }
}
