﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Gender = "Male"
            };

            Customer cust2 = new Customer()
            {
                ID = 2,
                Name = "Thando",
                Gender = "Female"
            };

            Customer cust3 = new Customer()
            {
                ID = 3,
                Name = "Lindi",
                Gender = "Female"
            };

            Customer cust4 = new Customer()
            {
                ID = 4,
                Name = "Thabiso",
                Gender = "Male"
            };

            Customer cust5 = new Customer()
            {
                ID = 5,
                Name = "Thoko",
                Gender = "Female"
            };

            Customer cust6 = new Customer()
            {
                ID = 6,
                Name = "Linzy",
                Gender = "Female"
            };

            Stack<Customer> stackOfCustomers = new Stack<Customer>();
            stackOfCustomers.Push(cust1);
            stackOfCustomers.Push(cust2);
            stackOfCustomers.Push(cust3);
            stackOfCustomers.Push(cust4);
            stackOfCustomers.Push(cust5);
            stackOfCustomers.Push(cust6);

            Customer result = stackOfCustomers.Peek();
            Console.WriteLine($"Id: {result.ID} Name: {result.Name} Gender: {result.Gender}");
            Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            bool answer = stackOfCustomers.Contains(cust6);
            if (answer)
            {
                Console.WriteLine("Yes,it contains the instance");
            }
            else
            {
                Console.WriteLine("It doesn't contain the instance");
            }

            //Customer result2 = stackOfCustomers.Pop();
            //Console.WriteLine($"Id: {result2.ID} Name: {result2.Name} Gender: {result2.Gender}");
            //Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            //Customer result3 = stackOfCustomers.Pop();
            //Console.WriteLine($"Id: {result3.ID} Name: {result3.Name} Gender: {result3.Gender}");
            //Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            //Customer result4 = stackOfCustomers.Pop();
            //Console.WriteLine($"Id: {result4.ID} Name: {result4.Name} Gender: {result4.Gender}");
            //Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            //Customer result5 = stackOfCustomers.Pop();
            //Console.WriteLine($"Id: {result5.ID} Name: {result5.Name} Gender: {result5.Gender}");
            //Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            //Customer result6 = stackOfCustomers.Pop();
            //Console.WriteLine($"Id: {result6.ID} Name: {result6.Name} Gender: {result6.Gender}");
            //Console.WriteLine($"Total count: {stackOfCustomers.Count}");

            //the foreach will only display the values but will not remove them from the queue
            //foreach (Customer item in stackOfCustomers)
            //{
            //    Console.WriteLine($"Id: {item.ID} Name: {item.Name} Gender: {item.Gender}");
            //    Console.WriteLine($"Total count: {stackOfCustomers.Count}");
            //}

            Console.ReadLine();
        }
    }
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }

    }
}
