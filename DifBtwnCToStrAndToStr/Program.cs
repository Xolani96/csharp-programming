﻿using System;

namespace DifBtwnCToStrAndToStr
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer c1 = new Customer();
            //string str = c1.ToString();
            //or
            string str = Convert.ToString(c1);
            Console.WriteLine(str);

            Console.ReadLine();
        }
    }
    public class Customer
    {
        public string Name { get; set; }
    }
}
