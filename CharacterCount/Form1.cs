﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace CharacterCount
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //count the number of characters in a file
        private int CountCharacters()
        {
            int Count = 0;
            //instance of stream reader
            using (StreamReader reader = new StreamReader(@"C:\Users\Nhlap\OneDrive\Desktop\FileCount.txt"))
            {
                string content = reader.ReadToEnd();
                Count = content.Length;
                Thread.Sleep(5000);
            }
            return Count;
        }
        int charactercount = 0;
        private /*async*/ void btnProcessFIle_Click(object sender, EventArgs e)
        {
            Thread T1 = new Thread(() =>
            {
                charactercount = CountCharacters();
                Action action = new Action(SetlabelTextPropety);
                this.BeginInvoke(action); // the beginInvoke will ask the UI to execute the code in "action" in a stype safe manner
            });
            T1.Start();

            lblCount.Text = "Processing file ,please wait...";
            //int count = await task; // the application will wait for the task to complete processing
        }

        private void SetlabelTextPropety()
        {
            lblCount.Text = charactercount.ToString() + " characters in the file";
        }
    }
}
