﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace ProtectingShared_Resources_Threading_
{
    class Program
    {
        static int Total = 0;
        static void Main(string[] args)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            Thread t1 = new Thread(Program.AddOneMillion);
            t1.Start();

            Thread t2 = new Thread(Program.AddOneMillion);
            t2.Start();

            Thread t3 = new Thread(Program.AddOneMillion);
            t3.Start();

            t1.Join(); t2.Join(); t3.Join();


            Console.WriteLine("Total: " + Total);
            stopwatch.Stop();
            Console.WriteLine("Time: " + stopwatch.ElapsedTicks);
            Console.ReadLine();
        }
        static object _lock = new object();
        public static void AddOneMillion()
        {
            for (int i = 1; i <= 1000000; i++)
            {
                //Total++; // total++ is not thread safe

                //solution1(Interlock)
                //Interlocked.Increment(ref Total);

                //Solution 2(Locking): only one thread can enter this piece of code.
                //lock (_lock)
                //{
                //    Total++;
                //}

                //Solution 3
                //Monitor.Enter(_lock); //  a thread enters and acquires an exclusive lock
                //try
                //{
                //    Total++;
                //}
                //finally
                //{
                //    Monitor.Exit(_lock);// release the exlcusive lock
                //}

                //Solution 4 : C# 4
                bool takenLock = false;
                Monitor.Enter(_lock,ref takenLock);
                try
                {
                    Total++;
                }
                finally
                {
                    if (takenLock)
                        Monitor.Exit(_lock);
                }
            }
        }
    }
}
