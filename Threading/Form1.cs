﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Threading
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void DTCW_btn_Click(object sender, EventArgs e)
        {
            DTCW_btn.Enabled = false;
            PrintNumber_btn.Enabled = false;

            //DoTimeConsumingWork();
            Thread workerThread = new Thread(DoTimeConsumingWork);

            DTCW_btn.Enabled = true;
            PrintNumber_btn.Enabled = true;
        }

        private void DoTimeConsumingWork()
        {
            Thread.Sleep(5000);
        }

        private void PrintNumber_btn_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                listBoxNumbers.Items.Add(i);
            }
        }
    }
}
