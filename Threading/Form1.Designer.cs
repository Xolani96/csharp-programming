﻿namespace Threading
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DTCW_btn = new System.Windows.Forms.Button();
            this.PrintNumber_btn = new System.Windows.Forms.Button();
            this.listBoxNumbers = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // DTCW_btn
            // 
            this.DTCW_btn.Location = new System.Drawing.Point(223, 68);
            this.DTCW_btn.Name = "DTCW_btn";
            this.DTCW_btn.Size = new System.Drawing.Size(297, 23);
            this.DTCW_btn.TabIndex = 0;
            this.DTCW_btn.Text = "Do time consuming work";
            this.DTCW_btn.UseVisualStyleBackColor = true;
            this.DTCW_btn.Click += new System.EventHandler(this.DTCW_btn_Click);
            // 
            // PrintNumber_btn
            // 
            this.PrintNumber_btn.Location = new System.Drawing.Point(223, 133);
            this.PrintNumber_btn.Name = "PrintNumber_btn";
            this.PrintNumber_btn.Size = new System.Drawing.Size(297, 23);
            this.PrintNumber_btn.TabIndex = 1;
            this.PrintNumber_btn.Text = "Print Numbers";
            this.PrintNumber_btn.UseVisualStyleBackColor = true;
            this.PrintNumber_btn.Click += new System.EventHandler(this.PrintNumber_btn_Click);
            // 
            // listBoxNumbers
            // 
            this.listBoxNumbers.FormattingEnabled = true;
            this.listBoxNumbers.ItemHeight = 16;
            this.listBoxNumbers.Location = new System.Drawing.Point(223, 182);
            this.listBoxNumbers.Name = "listBoxNumbers";
            this.listBoxNumbers.Size = new System.Drawing.Size(297, 228);
            this.listBoxNumbers.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listBoxNumbers);
            this.Controls.Add(this.PrintNumber_btn);
            this.Controls.Add(this.DTCW_btn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DTCW_btn;
        private System.Windows.Forms.Button PrintNumber_btn;
        private System.Windows.Forms.ListBox listBoxNumbers;
    }
}

