﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Thread_JoinNIsAlive
{
    class Program
    {
        static int Total = 0;
        static void Main(string[] args)
        {
            #region Part 92
            //Console.WriteLine("Main started");
            //Thread T1 = new Thread(Program.Thread1Function);
            //T1.Start();

            //Thread T2 = new Thread(Program.Thread2Function);
            //T2.Start();

            //if (T1.Join(1000))
            //{
            //    Console.WriteLine("Thread1Function completed");
            //}
            //else
            //{
            //    Console.WriteLine("Thread1Function is incompleted");
            //}

            //T2.Join();
            //Console.WriteLine("Thread2Function completed");

            //for (int i = 0; i < 10; i++)
            //{
            //    if (T1.IsAlive)
            //    {
            //        Console.WriteLine("Thread1Function is still doing it's work");
            //        Thread.Sleep(500);
            //    }
            //    else
            //    {
            //        Console.WriteLine("Thread1Function completed");
            //        break;
            //    } 
            //}

            //Console.WriteLine("Main completed");
            #endregion

            Stopwatch stopwatch = Stopwatch.StartNew();
            Thread T1 = new Thread(Program.AddOneMillion);
            Thread T2 = new Thread(Program.AddOneMillion);
            Thread T3 = new Thread(Program.AddOneMillion);

            T1.Start(); 
            T2.Start(); 
            T3.Start();

            T1.Join(); 
            T2.Join(); 
            T3.Join();

            Console.WriteLine("Total = " + Total);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedTicks);

            Console.ReadKey();
        }
        #region Part 92
        //public static void Thread1Function()
        //{
        //    Console.WriteLine("Thread 1 function is started");
        //    Thread.Sleep(5000);
        //    Console.WriteLine("Thread 1 function is about to return");
        //}
        //public static void Thread2Function()
        //{
        //    Console.WriteLine("Thread 2 function is started");
        //}
        #endregion

        //locking
        static object _lock = new object();
        public static void AddOneMillion()
        {
            for (int i = 1; i <= 1000000; i++)
            {
                //interlock.Increment()
                Interlocked.Increment(ref Total);

                //locking
                //lock (_lock)
                //{
                //    Total++;
                //}
            }
        }
    }
}
