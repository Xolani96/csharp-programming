﻿using System;
using System.Text; 

namespace SysStringVsSysTxtStrBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            //Immutable - means you cannot change the object in memory/ you cannot change them once created.

            /*String
            string userString = "C#";
            userString += " Video";
            userString += " are";
            userString += " best";
            userString += " Done";
            userString += " by";
            userString += " Wenkad";
            Console.WriteLine(userString.ToString());*/

            /*StringBuilder
            StringBuilder userString = new StringBuilder("C#");
            userString.Append(" Video");
            userString.Append(" are");
            userString.Append(" best");
            userString.Append(" Done");
            userString.Append(" by");
            userString.Append(" Wenkad");
            Console.WriteLine(userString.ToString());*/

            StringBuilder userString = new StringBuilder();
            for (int i = 0; i <= 100; i++)
            {
                userString.Append(i.ToString() + " "); 
            }
            Console.WriteLine(userString);

            Console.ReadKey();
        }
    }
}
