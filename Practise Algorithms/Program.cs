﻿using System;

namespace Practise_Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Old Practise
            Console.WriteLine(ADDITION(2, 5));
            Console.WriteLine(Absolute(60));
            Console.WriteLine(EqualTo30(30, 30));
            Console.ReadLine();

            //Factorial
            Console.WriteLine("Please enter a number: ");
            var value = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"The factorial of {value} is {FactorialLoop(value)}");
            #endregion

            Console.ReadKey();
        }
        #region Old Practise
        // Write a C# Sharp program to compute the sum of the two given integer values. If the two values are the same, then return triple their sum
        public static int ADDITION(int a, int b)
        {
            if (a == b)
            {
                return ((a + b) * 3);
            }
            else
            {
                return (a + b);
            }
        }
        // Write a C# Sharp program to get the absolute difference between n and 51. If n is greater than 51 return triple the absolute difference
        public static int Absolute(int n)
        {
            return n > 51 ? (Math.Abs(51 - n) * 3) : Math.Abs(51 - n);
        }
        // Write a C# Sharp program to check two given integers, and return true if one of them is 30 or if their sum is 30
        public static Boolean EqualTo30(int a, int b)
        {
            return (a == 30) || (b == 30) || ((a + b) == 30) ? true : false;
        }

        //Compute a Factorial using loops (algorithm)
        public static int FactorialLoop(int n)
        {
            var factorial = 1;

            //backward loop/descending
            //for (int i = n; i >=1 ; i--)
            //{
            //    factorial *= i;
            //}

            //forward loop/ascending
            for (int i = 1; i <= n; i++)
            {
                factorial *= i;
            }
            return factorial;
        }
        #endregion
    }
}
