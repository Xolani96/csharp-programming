﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace indexers
{
    public class Employee
    {
        int Eno;
        double Salary;
        string Ename, Job, Dname, Location;

        public Employee(int Eno, double Salary, string Ename, string Job, string Dname, string Location)
        {
            this.Eno = Eno;
            this.Salary = Salary;
            this.Ename = Ename;
            this.Job = Job;
            this.Dname = Dname;
            this.Location = Location;
        }

        public object this[int index]
        {
            get
            {
                //if (index == 0)
                //    return Eno;
                //else if (index == 1)
                //    return Salary;
                //else if (index == 2)
                //    return Ename;
                //else if (index == 3)
                //    return Job;
                //else if (index == 4)
                //    return Dname;
                //else if (index == 5)
                //    return Location;

                //return null;
                //or

                switch (index)
                {
                    case 0:
                        return Eno;
                        break;
                    case 1:
                        return Salary;
                        break;
                    case 2:
                        return Ename;
                        break;
                    case 3:
                        return Job;
                        break;
                    case 4:
                        return Dname;
                        break;
                    case 5:
                        return Location;
                        break;
                    default:
                        throw new Exception("Indexes are from 0 to 5");
                        break;
                }
            }

            set
            {
                if (index == 0)
                    Eno = (int)value;
                else if (index == 1)
                    Salary =(double)value;
                else if (index == 2)
                    Ename = (string)value;
                else if (index == 3)
                    Job = (string)value;
                else if (index == 4)
                    Dname = (string)value;
                else if (index == 5)
                    Location = (string)value;

            }
        }
        public object this[string name]
        {
            get
            {
                //if (index == 0)
                //    return Eno;
                //else if (index == 1)
                //    return Salary;
                //else if (index == 2)
                //    return Ename;
                //else if (index == 3)
                //    return Job;
                //else if (index == 4)
                //    return Dname;
                //else if (index == 5)
                //    return Location;

                //return null;
                //or

                switch (name)
                {
                    case "Eno":
                        return Eno;
                        break;
                    case "Salary":
                        return Salary;
                        break;
                    case "Ename":
                        return Ename;
                        break;
                    case "Job":
                        return Job;
                        break;
                    case "Dname":
                        return Dname;
                        break;
                    case "Location":
                        return Location;
                        break;
                    default:
                        throw new Exception("Indexes are from 0 to 5");
                        break;
                }
            }

            set
            {
                if (name.ToUpper() == "ENO")
                    Eno = (int)value;
                else if (name.ToUpper() == "SALARY")
                    Salary = (double)value;
                else if (name.ToUpper() == "ENAME")
                    Ename = (string)value;
                else if (name.ToUpper() == "JOB")
                    Job = (string)value;
                else if (name.ToUpper() == "DNAME")
                    Dname = (string)value;
                else if (name.ToUpper() == "LOCATION")
                    Location = (string)value;
            }
        }
    }
}