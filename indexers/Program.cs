﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace indexers
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp = new Employee(1001, 20000, "Xolani", "Programmer","Informatics","Budapest");

            Console.WriteLine("Eno: " + emp[0]);
            Console.WriteLine("Salary: " + emp[1]);
            Console.WriteLine("Ename: " + emp[2]);
            Console.WriteLine("Job: " + emp[3]);
            Console.WriteLine("Dname:" + emp[4]);
            Console.WriteLine("Location: "+ emp[5]);

            emp["Eno"] = 1023;
            emp["Salary"] = 25000.00;
            emp["Ename"] = "Steven";
            emp["Job"] = "Designer";
            emp["Dname"] = "Arts";
            emp["Location"] = "Debrecen";

            Console.WriteLine();

            Console.WriteLine("updated Eno: " + emp[0]);
            Console.WriteLine("updated Salary: " + emp[1]);
            Console.WriteLine("updated Ename: " + emp[2]);
            Console.WriteLine("updated Job: " + emp[3]);
            Console.WriteLine("updated Dname:" + emp[4]);
            Console.WriteLine("updated Location: " + emp[5]);

            Console.ReadKey();

        }
    }
}
