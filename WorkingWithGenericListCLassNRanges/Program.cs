﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithGenericListCLassNRanges
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<int> numbers = new List<int>() { 1, 8, 7, 5, 2, 3, 4, 9, 6 };
            //Console.WriteLine("Numbers before sorting");
            //foreach (int item in numbers)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine("Numbers after sorting");
            //numbers.Sort();
            //foreach (int item in numbers)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine("Numbers in descendinng order sorting");
            //numbers.Reverse();
            //foreach (int item in numbers)
            //{
            //    Console.WriteLine(item);
            //}

            //List<string> alphabets = new List<string>() { "A", "E", "D","F", "B", "G", "C" };
            //Console.WriteLine("alphabet before sorting");
            //foreach (string item in alphabets)
            //{
            //    Console.WriteLine(item);
            //}

            //alphabets.Sort();
            //Console.WriteLine("alphabet after sorting");
            //foreach (string item in alphabets)
            //{
            //    Console.WriteLine(item);
            //}

            //alphabets.Reverse();
            //Console.WriteLine("alphabet in descending order sorting");
            //foreach (string item in alphabets)
            //{
            //    Console.WriteLine(item);
            //}

            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Salary = 1000,
                Type = "RetailCustomer"
            };

            Customer cust2 = new Customer()
            {
                ID = 2,
                Name = "Simphiwe",
                Salary = 5500,
                Type = "RetailCustomer"
            };

            Customer cust3 = new Customer()
            {
                ID = 3,
                Name = "Kopano",
                Salary = 4511,
                Type = "RetailCustomer"
            };

            List<Customer> ListCustomer = new List<Customer>();
            ListCustomer.Add(cust1);
            ListCustomer.Add(cust2);
            ListCustomer.Add(cust3);

            ListCustomer.Sort();

            #region Part 77
            //Customer cust4 = new Customer()
            //{
            //    ID = 4,
            //    Name = "Wisley",
            //    Salary = 10000,
            //    Type = "CorporateCustomer"
            //};

            //Customer cust5 = new Customer()
            //{
            //    ID = 5,
            //    Name = "Simphiwe",
            //    Salary = 15000,
            //    Type = "CorporateCustomer"
            //};

            //List<Customer> ListCorporateCustomer = new List<Customer>();
            //ListCorporateCustomer.Add(cust4);
            //ListCorporateCustomer.Add(cust5);

            //Console.WriteLine("AddRange function");
            //ListCustomer.AddRange(ListCorporateCustomer);
            //foreach (Customer item in ListCustomer)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();

            //Console.WriteLine("GetRange function");
            //List<Customer> newList = ListCustomer.GetRange(3, 2);
            //foreach (Customer item in newList)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();

            //Console.WriteLine("Insert range function");
            //ListCustomer.InsertRange(2, ListCorporateCustomer);
            //foreach (Customer item in ListCustomer)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();
            //Console.ReadKey();

            //Console.WriteLine("Remove range function");
            //ListCustomer.RemoveRange(0,1);
            //foreach (Customer item in ListCustomer)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();
            //Console.ReadKey();

            //Console.WriteLine("Remove All function");
            //ListCustomer.RemoveAll(x => x.Type == "CorporateCustomer");
            //foreach (Customer item in ListCustomer)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();
            //Console.ReadKey();

            //Console.WriteLine("List Clear function");
            //ListCustomer.Clear();
            //foreach (Customer item in ListCustomer)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary} Type: {item.Type}");
            //}
            //Console.WriteLine();

            #endregion

            Console.ReadKey();
        }

        public class Customer
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int Salary { get; set; }
            public string Type { get; set; }
        }
    }
}
