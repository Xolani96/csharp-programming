﻿namespace Reflection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Methods_lstbox = new System.Windows.Forms.ListBox();
            this.Properties_lstbox = new System.Windows.Forms.ListBox();
            this.Constructor_lstbox = new System.Windows.Forms.ListBox();
            this.Methods_lbl = new System.Windows.Forms.Label();
            this.Properties_lbl = new System.Windows.Forms.Label();
            this.Constructor_lbl = new System.Windows.Forms.Label();
            this.TypeName_lbl = new System.Windows.Forms.Label();
            this.TypeName_txtbox = new System.Windows.Forms.TextBox();
            this.DiscTypInfo_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Methods_lstbox
            // 
            this.Methods_lstbox.FormattingEnabled = true;
            this.Methods_lstbox.Location = new System.Drawing.Point(21, 109);
            this.Methods_lstbox.Name = "Methods_lstbox";
            this.Methods_lstbox.Size = new System.Drawing.Size(223, 277);
            this.Methods_lstbox.TabIndex = 0;
            // 
            // Properties_lstbox
            // 
            this.Properties_lstbox.FormattingEnabled = true;
            this.Properties_lstbox.Location = new System.Drawing.Point(267, 109);
            this.Properties_lstbox.Name = "Properties_lstbox";
            this.Properties_lstbox.Size = new System.Drawing.Size(226, 277);
            this.Properties_lstbox.TabIndex = 1;
            // 
            // Constructor_lstbox
            // 
            this.Constructor_lstbox.FormattingEnabled = true;
            this.Constructor_lstbox.Location = new System.Drawing.Point(520, 109);
            this.Constructor_lstbox.Name = "Constructor_lstbox";
            this.Constructor_lstbox.Size = new System.Drawing.Size(236, 277);
            this.Constructor_lstbox.TabIndex = 2;
            // 
            // Methods_lbl
            // 
            this.Methods_lbl.AutoSize = true;
            this.Methods_lbl.Location = new System.Drawing.Point(97, 79);
            this.Methods_lbl.Name = "Methods_lbl";
            this.Methods_lbl.Size = new System.Drawing.Size(48, 13);
            this.Methods_lbl.TabIndex = 3;
            this.Methods_lbl.Text = "Methods";
            // 
            // Properties_lbl
            // 
            this.Properties_lbl.AutoSize = true;
            this.Properties_lbl.Location = new System.Drawing.Point(352, 79);
            this.Properties_lbl.Name = "Properties_lbl";
            this.Properties_lbl.Size = new System.Drawing.Size(54, 13);
            this.Properties_lbl.TabIndex = 4;
            this.Properties_lbl.Text = "Properties";
            // 
            // Constructor_lbl
            // 
            this.Constructor_lbl.AutoSize = true;
            this.Constructor_lbl.Location = new System.Drawing.Point(619, 79);
            this.Constructor_lbl.Name = "Constructor_lbl";
            this.Constructor_lbl.Size = new System.Drawing.Size(61, 13);
            this.Constructor_lbl.TabIndex = 5;
            this.Constructor_lbl.Text = "Constructor";
            // 
            // TypeName_lbl
            // 
            this.TypeName_lbl.AutoSize = true;
            this.TypeName_lbl.Location = new System.Drawing.Point(26, 25);
            this.TypeName_lbl.Name = "TypeName_lbl";
            this.TypeName_lbl.Size = new System.Drawing.Size(62, 13);
            this.TypeName_lbl.TabIndex = 6;
            this.TypeName_lbl.Text = "Type Name";
            // 
            // TypeName_txtbox
            // 
            this.TypeName_txtbox.Location = new System.Drawing.Point(108, 22);
            this.TypeName_txtbox.Name = "TypeName_txtbox";
            this.TypeName_txtbox.Size = new System.Drawing.Size(241, 20);
            this.TypeName_txtbox.TabIndex = 7;
            // 
            // DiscTypInfo_btn
            // 
            this.DiscTypInfo_btn.Location = new System.Drawing.Point(430, 20);
            this.DiscTypInfo_btn.Name = "DiscTypInfo_btn";
            this.DiscTypInfo_btn.Size = new System.Drawing.Size(293, 23);
            this.DiscTypInfo_btn.TabIndex = 8;
            this.DiscTypInfo_btn.Text = "Discover Type Information";
            this.DiscTypInfo_btn.UseVisualStyleBackColor = true;
            this.DiscTypInfo_btn.Click += new System.EventHandler(this.DiscTypInfo_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 405);
            this.Controls.Add(this.DiscTypInfo_btn);
            this.Controls.Add(this.TypeName_txtbox);
            this.Controls.Add(this.TypeName_lbl);
            this.Controls.Add(this.Constructor_lbl);
            this.Controls.Add(this.Properties_lbl);
            this.Controls.Add(this.Methods_lbl);
            this.Controls.Add(this.Constructor_lstbox);
            this.Controls.Add(this.Properties_lstbox);
            this.Controls.Add(this.Methods_lstbox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Methods_lstbox;
        private System.Windows.Forms.ListBox Properties_lstbox;
        private System.Windows.Forms.ListBox Constructor_lstbox;
        private System.Windows.Forms.Label Methods_lbl;
        private System.Windows.Forms.Label Properties_lbl;
        private System.Windows.Forms.Label Constructor_lbl;
        private System.Windows.Forms.Label TypeName_lbl;
        private System.Windows.Forms.TextBox TypeName_txtbox;
        private System.Windows.Forms.Button DiscTypInfo_btn;
    }
}

