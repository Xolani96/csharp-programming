﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Reflection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //System.Console
        private void DiscTypInfo_btn_Click(object sender, EventArgs e)
        {
            string TypeName = TypeName_txtbox.Text;
            Type T = Type.GetType(TypeName);

            //clear the list box before displaying.
            Methods_lstbox.Items.Clear();
            Properties_lstbox.Items.Clear();
            Constructor_lstbox.Items.Clear();

            //methods to display properties, methods and constructor of a Type
            MethodInfo[] Methods = T.GetMethods();
            foreach (MethodInfo method in Methods)
            {
                Methods_lstbox.Items.Add(method.ReturnType.Name +" "+ method.Name);
            }

            PropertyInfo[] Properties = T.GetProperties();
            foreach (PropertyInfo property in Properties)
            {
                Properties_lstbox.Items.Add(property.PropertyType.Name + " " + property.Name);
            }

            ConstructorInfo[] Constructors = T.GetConstructors();
            foreach (ConstructorInfo Constructor in Constructors)
            {
                Constructor_lstbox.Items.Add(Constructor.ToString());
            }
        }
    }
}
