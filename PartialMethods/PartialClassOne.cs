﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialMethods
{
    public partial class SamplePartialClass
    {
        //definition of the method
        partial void SamplePartialMethod();
        //implementation of the method
        partial void SamplePartialMethod()
        {
            Console.WriteLine("Sample partial method is Invoked");
        }
        public void PublicMethod()
        {
            Console.WriteLine("Public method Invoked");
            SamplePartialMethod();
        }
    }
}
