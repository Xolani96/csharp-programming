﻿using System;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            bool Equal = Calculator<int>.AreEquals/*<int>*/(10, 10);

            //display answer
            if (Equal)
            {
                Console.WriteLine("The numbers are equal");
            }
            else
            {
                Console.WriteLine("The numbers are not equal");
            }

            Console.ReadLine();
        }

        public class Calculator<T> // makes the whole class generic
        {
            public static bool AreEquals/*<T>*/(T num1, T num2) // compares two number and returns true or false depending on the result
            {
                return num1.Equals(num2);
            }
        }
    }
}
