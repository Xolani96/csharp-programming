﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadStart_Delegate
{
    public delegate void SumOfNumbersCallBack(int SumOfNumbers);
    class Program
    {
        public static void PrintSum(int sum)
        {
            Console.WriteLine("Sum of numbers is " + sum);
        }
        static void Main(string[] args)
        {
            //ways to initialize a thread

            //1. normal 
            Console.WriteLine("Please enter a target number?");
            int target = Convert.ToInt32(Console.ReadLine());

            SumOfNumbersCallBack callback = new SumOfNumbersCallBack(PrintSum);

            Number number = new Number(target,callback);
            //ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(Number.PrintNumber);

            //threading
            Thread thread1 = new Thread(new ThreadStart(number.PrintSumOfNumber));

            //2. using ThreadStart
            //Thread thread1 = new Thread(new ThreadStart(Number.PrintNumber));
            //3. using delegates
            //Thread thread1 = new Thread(delegate() { Number.PrintNumber(); });
            //4. using Lambda expressions
            //Thread thread1 = new Thread(() => Number.PrintNumber());

            thread1.Start(/*target*/);

            Console.ReadKey();
        }
    }
    class Number
    {
        int _target;
        SumOfNumbersCallBack _callBackMethod;
        public Number(int target, SumOfNumbersCallBack callBackMethod)
        {
            this._target = target;
            this._callBackMethod = callBackMethod;
        }

        //Parametrized ThradStart delegate
        //public static void PrintNumber(object target)
        //{
        //    int number = 0;

        //    if (int.TryParse(target.ToString(), out number))
        //    {
        //        for (int i = 1; i <= number; i++)
        //        {
        //            Console.WriteLine(i);
        //        } 
        //    }
        //}

        // Passing data to the thread function in a safe manner.
        public void PrintSumOfNumber()
        {
            int sum = 0;
            for (int i = 1; i <= _target ; i++)
            {
                sum += i;
            }

            if (_callBackMethod != null)
                _callBackMethod(sum);

        }
    }
}
