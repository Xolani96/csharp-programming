﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryVSList
{
    class Program
    {
        static void Main(string[] args)
        {
            Country country1 = new Country() { Code = "ZAR", Name = "South Africa", Capital = "Pretoria" };
            Country country2 = new Country() { Code = "DZA", Name = "Algeria", Capital = "Algiers" };
            Country country3 = new Country() { Code = "AGO", Name = "Angola", Capital = "Luanda" };
            Country country4 = new Country() { Code = "BEN", Name = "Benin", Capital = "Porto-Novo" };
            Country country5 = new Country() { Code = "EGY", Name = "Egypt", Capital = "Cairo" };
            Country country6 = new Country() { Code = "KEN", Name = "Kenya", Capital = "Nairobi" };
            Country country7 = new Country() { Code = "MDG", Name = "Madagascar", Capital = "Antananarivo" };

            #region Using a List
            //using a list 
            //List<Country> listCountry = new List<Country>();
            //listCountry.Add(country1);
            //listCountry.Add(country2);
            //listCountry.Add(country3);
            //listCountry.Add(country4);
            //listCountry.Add(country5);
            //listCountry.Add(country6);
            //listCountry.Add(country7);

            //string answer = string.Empty;

            //do
            //{

            //    Console.WriteLine("Please enter the country code ");
            //    string CountryCode = Console.ReadLine().ToUpper();

            //    Country foundcountry = listCountry.Find(country => country.Code == CountryCode);

            //    if (foundcountry == null)
            //    {
            //        Console.WriteLine("Country code is invalid");
            //    }
            //    else
            //    {
            //        Console.WriteLine($"Name: {foundcountry.Name} Capital: {foundcountry.Capital} Country Code: {foundcountry.Code}");
            //    }

            //    do
            //    {
            //        Console.WriteLine("Do you wish to continue - YES or NO ? ");
            //        answer = Console.ReadLine().ToUpper();
            //    } while (!(answer == "YES" || answer == "NO")); 
            //} while( answer == "YES");
            #endregion
            #region Usind a Dictionary
            Dictionary<string, Country> DictionaryCountry = new Dictionary<string, Country>();
            DictionaryCountry.Add(country1.Code, country1);
            DictionaryCountry.Add(country2.Code, country2);
            DictionaryCountry.Add(country3.Code, country3);
            DictionaryCountry.Add(country4.Code, country4);
            DictionaryCountry.Add(country5.Code, country5);
            DictionaryCountry.Add(country6.Code, country6);
            DictionaryCountry.Add(country7.Code, country7);

            string answer = string.Empty;

            do
            {

                Console.WriteLine("Please enter the country code ");
                string CountryCode = Console.ReadLine().ToUpper();

                Country foundcountry = DictionaryCountry.ContainsKey(CountryCode) ? DictionaryCountry[CountryCode] : null; 

                if (foundcountry == null)
                {
                    Console.WriteLine("Country code is invalid");
                }
                else
                {
                    Console.WriteLine($"Name: {foundcountry.Name} Capital: {foundcountry.Capital} Country Code: {foundcountry.Code}");
                }

                do
                {
                    Console.WriteLine("Do you wish to continue - YES or NO ? ");
                    answer = Console.ReadLine().ToUpper();
                } while (!(answer == "YES" || answer == "NO"));
            } while (answer == "YES");

            #endregion
            Console.ReadLine();
        }
    }
    public class Country
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
    }
}
