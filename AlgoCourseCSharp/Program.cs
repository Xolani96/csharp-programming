﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoCourseCSharp
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        static void ArrayTimeComplexity( object[] array)
        {
            //access by index O(1) if you know the location of the element
            Console.WriteLine(array[0]);

            //searching ffor an element O(n) .if you don't know the location of the element, so you need to search the whole array first to find the element
            int lenght = array.Length;
            object elementINeedToFind = new object();
            for (int i = 0; i < lenght; i++)
            {
                if(array[i] == elementINeedToFind)
                {
                    Console.WriteLine($"Found......Element : {array[i]}");
                }
            }

            //Add to a full array
            var BigArray = new int[lenght * 2]; // double the old array and make that the size of the new array
            Array.Copy(array, BigArray, lenght);
            //adding an element to the big array
            BigArray[lenght + 1] = 10;

            //add to the end when there is some space. O(1)
            array[lenght - 1] = 10;

            //O(1), removing an element by assinging null
            array[4] = null;
        }

        private static void RemoveAt(object[] array, int index)
        {
            var newArray = new int[array.Length - 1];
            Array.Copy(array, 0, newArray, 0, index);
            Array.Copy(array, index + 1, newArray, index, array.Length - 1 - index);
        }

    }
}
