﻿using System;

namespace OverrideToString
{
    class Program
    {
        static void Main(string[] args)
        {
            //int number = 10;
            //Console.WriteLine(number);

            Customer c = new Customer();
            c.FirstName = "Xolani";
            c.Lastname = "Nhlapo";

            Console.WriteLine(c.ToString()); //or Convert.ToString(c)

            Console.ReadKey();
        }
    }

    public class Customer
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }

        public override string ToString()
        {
            return this.Lastname + " " + this.FirstName;
        }
    }
}
