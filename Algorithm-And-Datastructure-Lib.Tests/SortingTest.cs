﻿using AlgorithmAndDataStructureLibrary;
using NUnit.Framework;
using System;

namespace Algorithm_And_Datastructure_Lib.Tests
{
    [TestFixture]
    public class SortingTest
    {
        private int[][] Samples()
        {
            int[][] samples = new int[9][];
            samples[0] = new[] { 1 };
            samples[1] = new[] { 2, 1 };
            samples[2] = new[] { 2, 1, 3 };
            samples[3] = new[] { 1, 1, 1 };
            samples[4] = new[] { 5, -1, 3, 6 };
            samples[5] = new[] { 12, 4, 2, -4 };
            samples[6] = new[] { 25, -7, 6, 1 };
            samples[7] = new[] { 7, 4, 2, 9 };
            samples[8] = new[] { 7, 0, 4, 1, 8, 3, 2, 9 };

            return samples; 
        }

        private void RunTestFOrSortAlgo(Action<int[]> sort)
        {
            foreach (var sample in Samples())
            {
                sort(sample); // sorts the items in the array 
                CollectionAssert.IsOrdered(sample); // checks if the items are ordered
                PrintOut(sample);
            }
        }

        private void PrintOut(int[] array)
        {
            TestContext.WriteLine("-------Trace-------\n"); //It is a method that allows us to look at the written line in Visual Studio
            foreach (var el in array)
            {
                TestContext.Write(el + " ");
            }
            TestContext.WriteLine("\n--------------\n");
        }

        [Test]
        public void BubbleSort_ValidInput_Sorte4dInput()
        {
            RunTestFOrSortAlgo(Sorting.BubbleSort);
        }
        [Test]
        public void Sum_4and6_Return10()
        {
            var result = Sorting.Sum(4, 6);
            Assert.That(result, Is.EqualTo(10));
        }
    }
}
