﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Gender = "Male"
            };

            Customer cust2 = new Customer()
            {
                ID = 2,
                Name = "Thando",
                Gender = "Female"
            };

            Customer cust3 = new Customer()
            {
                ID = 3,
                Name = "Lindi",
                Gender = "Female"
            };

            Customer cust4 = new Customer()
            {
                ID = 4,
                Name = "Thabiso",
                Gender = "Male"
            };

            Customer cust5 = new Customer()
            {
                ID = 5,
                Name = "Thoko",
                Gender = "Female"
            };

            Customer cust6 = new Customer()
            {
                ID = 6,
                Name = "Linzy",
                Gender = "Female"
            };

            Queue<Customer> queueCustomers = new Queue<Customer>();
            //to insert an item to the queue
            queueCustomers.Enqueue(cust1);
            queueCustomers.Enqueue(cust2);
            queueCustomers.Enqueue(cust3);
            queueCustomers.Enqueue(cust4);
            queueCustomers.Enqueue(cust5);

            if (queueCustomers.Contains(cust6))
            {
                Console.WriteLine("Yes it contains the customer object");
            }
            else
            {
                Console.WriteLine("No it does not contain the customer object.");
            }
            //To retrive an item from a queue
            //Customer result = queueCustomers.Dequeue(); // the first item will be removed from the queue and then returned
            //Console.WriteLine($"Id: {result.ID} Name: {result.Name} Gender: {result.Gender}");
            //Console.WriteLine($"Total count: {queueCustomers.Count}");

            //The peek method only shows the first object in the queue but does not remove it form the queue
            //Customer item = queueCustomers.Peek();
            //Console.WriteLine($"Id: {item.ID} Name: {item.Name} Gender: {item.Gender}");
            //Console.WriteLine($"Total count: {queueCustomers.Count}");

            //Customer item2 = queueCustomers.Peek();
            //Console.WriteLine($"Id: {item2.ID} Name: {item2.Name} Gender: {item2.Gender}");
            //Console.WriteLine($"Total count: {queueCustomers.Count}");

            //the foreach will only display the values but will not remove them from the queue
            //foreach (Customer item in queueCustomers)
            //{
            //    Console.WriteLine($"Id: {item.ID} Name: {item.Name} Gender: {item.Gender}");
            //    Console.WriteLine($"Total count: {queueCustomers.Count}");
            //}

            Console.ReadLine();
        }
    }
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
       
    }
}
