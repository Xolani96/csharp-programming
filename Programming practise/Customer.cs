﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programming_practise
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public Customer(int ID, string Name)
        {
            this.Id = ID;
            this.Name = Name;
        }

        public Customer()
        {
            this.Id = -1;
            this.Name = string.Empty;
        }

        public void PrintId()
        {
            Console.WriteLine("Id = {0}", this.Id);
        }

        public void PrintName()
        {
            Console.WriteLine("Name = {0}", this.Name);
        }
    }
}
