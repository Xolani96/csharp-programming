﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace Programming_practise
{
    class Program
    {
        static void Main(string[] args)
        {
            //Type t = Type.GetType("Programming_practise.Customer");
            //or
            Customer c1 = new Customer();
            Type t = c1.GetType();
            //or
            //Type t = typeof(Customer);// have to pass the class name


            Console.WriteLine("Full Name = {0}", t.FullName);
            Console.WriteLine("Name = {0}", t.Name);
            Console.WriteLine("Namespace = {0}", t.Namespace);

            Console.WriteLine();

            Console.WriteLine("Properties in customer class");
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine(property.PropertyType.Name + " " + property.Name);
            }

            Console.WriteLine();

            Console.WriteLine("Methods in customer class");
            MethodInfo[] Methods = t.GetMethods();
            foreach (MethodInfo Method in Methods)
            {
                Console.WriteLine(Method.ReturnType.Name + " " + Method.Name);
            }

            Console.WriteLine();

            Console.WriteLine("Constructors in customer class");
            ConstructorInfo[] Constructors = t.GetConstructors();
            foreach (ConstructorInfo Constructor in Constructors)
            {
                Console.WriteLine(Constructor.ToString());
            }

            //attributes
            //Calculator.Add(new List<int>() { 10,20,40 });

            //Gender gender = (Gender)Season.Winter;

            //Gender gender = (Gender)3;
            //int Num = (int)Gender.Unknown;

            //Customer[] customers = new Customer[3];

            //customers[0] = new Customer
            //{
            //    Name = "Mark",
            //    Gender = Gender.Male
            //};

            //customers[1] = new Customer
            //{
            //    Name = "Thembi",
            //    Gender = Gender.Female
            //};

            //customers[2] = new Customer
            //{
            //    Name = "Lindi",
            //    Gender = Gender.Unknown
            //};

            //foreach (Customer customer in customers)
            //{
            //    Console.WriteLine("Name = {0} \nGender = {1}", customer.Name, GetGender(customer.Gender));
            //}

            //short[] Values = (short[])Enum.GetValues(typeof(Gender));
            //foreach (short value in Values)
            //{
            //    Console.WriteLine(value);
            //}

            //string[] Names = (string[])Enum.GetNames(typeof(Gender));
            //foreach (string name in Names)
            //{
            //    Console.WriteLine(name);
            //}

            Console.ReadKey();
        }

        //public static string GetGender(Gender gender)
        //{
        //    switch (gender)
        //    {
        //        case Gender.Unknown:
        //            return "Unknown";
        //        case Gender.Male:
        //            return "Male";
        //        case Gender.Female:
        //            return "Female";
        //        default:
        //            throw new Exception("Invalid gender ,please pick between 0,1 and 2");
        //    }
        //}
    }

    public enum Gender /*: short*/ // change the underlyung type of an enum
    {
        Unknown = 1,
        Male /*= 5*/,
        Female /*= 23*/
        // Enum- it is a class, enum- key word to create an enumerations class
    }

    public enum Season
    {
        Winter =1,
        Spring = 2,
        Summer = 3
    }
    // 0 - Unknown
    //1 - Male
    //2 - Female
    //public class Customer
    //{
    //    public string Name { get; set; }
    //    public Gender Gender { get; set; }
    //}

}
