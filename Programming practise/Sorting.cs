﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programming_practise
{
    public class Sorting
    {
        public static void BubbleSort(int[] array)
        {
            for (int partIndex = array.Length - 1; partIndex > 0; partIndex--)
            {
                for (int i = 0; i < partIndex; i++)
                {
                    if(array[i] > array[i + 1])
                    {
                        Swap(array, i, i+1);
                    }
                }
            }
        }
        public static void Swap(int[] array, int i, int j)
        {
            if (i == j)
                return;
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
}
