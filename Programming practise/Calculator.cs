﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programming_practise
{
    public class Calculator
    {
        [Obsolete("Use Add(List<int> Numbers) Method", true)] // true - means if anyone wants to use this method, just throw an error
        public static int Add(int firstNum , int secondNum)
        {
            return firstNum + secondNum;
        }
        public static int Add(List<int> Numbers)
        {
            int sum = 0;
            foreach (int number in Numbers)
            {
                sum = sum + number;
            }
            return sum;
        }
    }
}
