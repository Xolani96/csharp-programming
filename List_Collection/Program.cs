﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Collection
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Salary = 1000
            };

            Customer cust2 = new Customer()
            {
                ID = 2,
                Name = "Simphiwe",
                Salary = 5500
            };

            Customer cust3 = new Customer()
            {
                ID = 3,
                Name = "Kopano",
                Salary = 4511
            };

            Customer cust4 = new Customer()
            {
                ID = 4,
                Name = "Wisley",
                Salary = 10000
            };

            Customer cust5 = new Customer()
            {
                ID = 5,
                Name = "Simphiwe",
                Salary = 15000
            };

            //List (the size of the list can be extended, it doesn't have a fixed size)
            //List<Customer> customers = new List<Customer>(2);
            //customers.Add(cust1);
            //customers.Add(cust2);
            //customers.Add(cust3); // present at the last index position
            //customers.Add(cust4);
            //customers.Add(cust5);

            //Console.WriteLine("--------------contains function--------------------");
            //if (customers.Contains(cust3))
            //{
            //    Console.WriteLine("Customer 3 object exists");
            //}
            //else
            //{
            //    Console.WriteLine("Customer 3 object doesnot exists");
            //}

            //Console.WriteLine("--------------Exists function--------------------");
            //if (customers.Exists(cust => cust.Name.StartsWith("X")))
            //{
            //    Console.WriteLine("Customer object whose name starts with X exists");
            //}
            //else
            //{
            //    Console.WriteLine("Customer object whose name starts with X doesnot exists");
            //}
            //Console.WriteLine();

            //Console.WriteLine("--------------find function--------------------");
            //Customer c = customers.Find(cust => cust.Salary > 4000);
            //Console.WriteLine($"ID: {c.ID} Name: {c.Name} Salary: {c.Salary}");
            //Console.WriteLine();

            //Console.WriteLine("--------------findLast function--------------------");
            //Customer d = customers.FindLast(cust => cust.Salary > 4000);
            //Console.WriteLine($"ID: {d.ID} Name: {d.Name} Salary: {d.Salary}");
            //Console.WriteLine();

            //Console.WriteLine("--------------findall function--------------------");
            //List<Customer> e = customers.FindAll(cust => cust.Salary > 4000);
            //foreach (var item in e)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary}");
            //}
            //Console.WriteLine();

            //Console.WriteLine("--------------findIndex function--------------------");
            //int f = customers.FindIndex(cust => cust.Name.Equals("Simphiwe"));
            //Console.WriteLine($"ID: {f}");

            //Console.WriteLine("--------------findLastIndex function--------------------");
            //int g = customers.FindLastIndex(0, 2, cust => cust.Name.Equals("Simphiwe"));
            //Console.WriteLine($"ID: {g}");
            //Console.WriteLine();

            //Console.WriteLine("--------------Convert an array to list--------------------");
            //Customer[] customersArray = new Customer[3];
            //customersArray[0] = cust1;
            //customersArray[1] = cust2;
            //customersArray[2] = cust3;

            //List<Customer> customerList= customersArray.ToList();
            //foreach (Customer item in customerList)
            //{
            //    Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary}");
            //}

            Console.WriteLine("--------------Convert a list to array--------------------");
            List<Customer> customers = new List<Customer>(2);
            customers.Add(cust1);
            customers.Add(cust2);
            customers.Add(cust3);
            customers.Add(cust4);
            customers.Add(cust5);

            Customer[] cust = customers.ToArray();
            foreach (Customer item in cust)
            {
                Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary}");
            }
            Console.WriteLine();

            Console.WriteLine("--------------Convert a list to dictionary--------------------");

            Dictionary<int, Customer> dictionarylist = customers.ToDictionary( x => x.ID);
            foreach (var item in dictionarylist.Values)
            {
                Console.WriteLine($"ID: {item.ID} Name: {item.Name} Salary: {item.Salary}");
            }


            #region List (part 74)
            //insert an item/object to a specific location
            //customers.Insert(0, cust3); // present at the first index position

            //Console.WriteLine(customers.IndexOf(cust3, 1, 3)); //(datatype, index you want to find, numbers of times you want to move in the list)

            //retreaving items from the list using a *for* loop
            //for (int i = 0; i < customers.Count; i++)
            //{
            //    Customer c = customers[i];
            //    Console.WriteLine($"ID: {c.ID} Name: {c.Name} Salary: {c.Salary}");
            //}

            //retreaving items from the list using a *foreach* loop
            //foreach (Customer c in customers)
            //{
            //    Console.WriteLine($"ID: {c.ID} Name: {c.Name} Salary: {c.Salary}");
            //}

            //retreaving items from list we use their index, index is 0 based meaning it starts from 0
            //Customer c =  customers[0];
            //Console.WriteLine($"ID: {c.ID} Name: {c.Name} Salary: {c.Salary}");

            //array (the size of the array cannot be extended, it has a fixed size)
            //Customer[] customers = new Customer[2];

            //customers[0] = cust1;
            //customers[1] = cust2;
            //customers[3] = cust3;

            //SavingsCustomer sc = new SavingsCustomer();
            //customers.Add(sc);
            #endregion

            Console.ReadKey();
        }
    }
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
    }
    public class SavingsCustomer: Customer
    {

    }
}
