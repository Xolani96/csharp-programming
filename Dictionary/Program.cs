﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer cust1 = new Customer()
            {
                ID = 1,
                Name = "Xolani",
                Salary = 1000
            };

            Customer cust2 = new Customer()
            {
                ID = 2,
                Name = "Simphiwe",
                Salary = 1500
            }; 

            Customer cust3 = new Customer()
            {
                ID = 3,
                Name = "Kopano",
                Salary = 4511
            };

            //convert a list to dictionary
            List<Customer> cust = new List<Customer>();
            cust.Add(cust1);
            cust.Add(cust2);
            cust.Add(cust3);

            Dictionary<int, Customer> dict = cust.ToDictionary(customer => customer.ID, customer => customer); // the kex is the customer ID and the value will be the customer object 
            foreach (KeyValuePair<int, Customer> keyValuePair in dict)
            {
                Console.WriteLine($"Key: {keyValuePair.Key} ");
                Customer customr = keyValuePair.Value;
                Console.WriteLine($"Key: {customr.ID} Name: {customr.Name} Salary: {customr.Salary}");
            }

            //convert array to dictionary
            //Customer[] cust = new Customer[3];
            //cust[0] = cust1;
            //cust[1] = cust2;
            //cust[2] = cust3;

            //Dictionary<int,Customer> dict = cust.ToDictionary(customer => customer.ID, customer => customer); // the kex is the customer ID and the value will be the customer object 
            //foreach (KeyValuePair<int, Customer> keyValuePair in dict)
            //{
            //    Console.WriteLine($"Key: {keyValuePair.Key} ");
            //    Customer customr = keyValuePair.Value;
            //    Console.WriteLine($"Key: {customr.ID} Name: {customr.Name} Salary: {customr.Salary}");
            //}

            //Dictionary

            //Dictionary<int, Customer> dictionaryCust = new Dictionary<int, Customer>();
            //dictionaryCust.Add(cust1.ID, cust1);
            //dictionaryCust.Add(cust2.ID, cust2);
            //dictionaryCust.Add(cust3.ID, cust3);

            //part 2

            //Clear
            //dictionaryCust.Clear();

            //remove()
            //dictionaryCust.Remove(1);

            //Count
            //Console.WriteLine($"Total: {dictionaryCust.Count(kvp => kvp.Value.Salary > 4000)}"); 

            //TryGetValue
            //Customer cust;
            //if (dictionaryCust.TryGetValue(10, out cust))
            //{
            //    Console.WriteLine($"ID: {cust.ID} Name: {cust.Name} Salary: {cust.Salary}");
            //}
            //else
            //{
            //    Console.WriteLine("Key does not exist within the dictionary");
            //}

            //part 1
            //if (!dictionaryCust.ContainsKey(cust1.ID))
            //{
            //    dictionaryCust.Add(cust1.ID, cust3);
            //}
            ////access the values using keys
            //Customer customer1 = dictionaryCust[1];
            ////or
            ////loop through the list of values and display them
            ////Console.WriteLine($"ID: {customer1.ID} Name: {customer1.Name} Salary : {customer1.Salary}");
            //foreach (/*KeyValuePair<int, Customer>*/ /*var*/ /*int*/ Customer customerKeyValuePair in dictionaryCust.Values) // its recommended to us KeyValuePair instead of var because it shows you the datatype of the value
            //{
            //    //Console.WriteLine(customerKeyValuePair);

            //    //    Console.WriteLine($"Key: {customerKeyValuePair.Key} ");
            //    //    Customer cust = customerKeyValuePair.Value;
            //    Console.WriteLine("ID: {0} Name: {1} Salary: {2}", customerKeyValuePair.ID, customerKeyValuePair.Name, customerKeyValuePair.Salary);
            //    //    Console.WriteLine("---------------------------------------------------");
            //}

            Console.ReadLine();
        }
    }
}
