﻿using System;
using System.Reflection;

namespace EarlyLateBinding
{
    class Program
    {
        static void Main(string[] args)
        {
            //late binding
            //create an instance of customer class
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            Type CustomerType = executingAssembly.GetType("EarlyLateBinding.Customers");
            object customerinstance = Activator.CreateInstance(CustomerType); // it will create an instance of the customer type
            //get the method in the class
            MethodInfo getFullNameMethod =  CustomerType.GetMethod("GetFullName"); //gets method information, you need to create the parameters of the method you want to instantiate
            //prepare the parameters
            string[] parameters = new string[2];
            parameters[0] = "Xolani"; // first parameter of the method ( name )
            parameters[1] = "Nhlapo"; //second parameter of the method
            //Invoke the method
            string fullname = (string)getFullNameMethod.Invoke(customerinstance, parameters);
            Console.WriteLine($"Full name is : {fullname}");

            //Early binding
            //Customers c1 = new Customers();
            //string fullname = c1.GetFullName("Xolani", "Nhlapo");
            //Console.WriteLine($"Full name is : {fullname}");
            //Console.ReadLine();
        }
    }

    public class Customers
    {   
        public string GetFullName(string FirstName, string LastName)
        {
            return FirstName + " " + LastName;
        }
    }
}
